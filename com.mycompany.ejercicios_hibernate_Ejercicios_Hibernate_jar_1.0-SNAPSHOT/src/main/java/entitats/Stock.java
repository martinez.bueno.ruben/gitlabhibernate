/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entitats;

import com.github.javafaker.Faker;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.exception.ConstraintViolationException;
import entitats.Stock;
import java.io.Serializable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
//Codigo ejercicio Hibernate
/**
 *
 * @author Ruben
 */
@Entity()
@Table(name = "Tabla_Alumnos")
public class Stock implements Serializable {

    public static void main(String[] args) {
        SessionFactory factory = new Configuration().configure().buildSessionFactory();
        Session session = factory.openSession();
        for (int i = 0; i < 1000; i++) {
            session.getTransaction().begin();
            Faker faker = new Faker();
            String nom = faker.name().name();
            String cognom = faker.name().firstName();
            String id = faker.idNumber().valid();
            Stock stock = new Stock(nom,cognom);
            session.persist(stock);
            session.getTransaction().commit();
        }
        
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "STOCK_ID", nullable = false)
    private Integer stockId;

    @Column(name = "STOCK_CODE", nullable = false)
    private String stockCode;
    @Column(name = "STOCK_NAME", nullable = false)
    private String stockName;

    public Stock() {
    }

    public Stock(Integer stockId) {
        this.stockId = stockId;
    }

    public Stock(String stockCode, String stockName) {
        this.stockCode = stockCode;
        this.stockName = stockName;
    }

    /**
     * @return the stockId
     */
    public Integer getStockID() {
        return stockId;
    }

    /**
     * @param stockID the stockId to set
     */
    public void setStockID(Integer stockID) {
        this.stockId = stockID;
    }

    /**
     * @return the stockCode
     */
    public String getStockCode() {
        return stockCode;
    }

    /**
     * @param stockCode the stockCode to set
     */
    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    /**
     * @return the stockName
     */
    public String getStockName() {
        return stockName;
    }

    /**
     * @param stockName the stockName to set
     */
    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Stock)) {
            return false;
        }
        Stock other = (Stock) object;
        if ((this.stockId == null && other.stockId != null) || (this.stockId != null && !this.stockId.equals(other.stockId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entitats.Stock[ stockId=" + stockId + " ]";
    }
}
